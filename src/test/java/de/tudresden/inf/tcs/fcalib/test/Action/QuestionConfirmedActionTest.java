package de.tudresden.inf.tcs.fcalib.test.Action;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.action.AbstractExpertAction;
import de.tudresden.inf.tcs.fcalib.action.ChangeAttributeOrderAction;
import de.tudresden.inf.tcs.fcalib.action.QuestionConfirmedAction;
import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.mockito.internal.matchers.Null;
public class QuestionConfirmedActionTest {

    QuestionConfirmedAction questionConfirmedAction;

    @Before
    public void init()
    {
        questionConfirmedAction = new QuestionConfirmedAction();
    }


    @Test (expected = Exception.class)
    public void testActionPerformed()
    {
        ActionEvent actionEvent = new ActionEvent("s",2,"n");
        questionConfirmedAction.actionPerformed(actionEvent);

    }

    @Test (expected = Exception.class)
    public void testActionPerformedNull()

    {
        questionConfirmedAction.actionPerformed(null);
    }

}
