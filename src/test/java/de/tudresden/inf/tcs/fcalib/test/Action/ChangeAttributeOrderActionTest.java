package de.tudresden.inf.tcs.fcalib.test.Action;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

import de.tudresden.inf.tcs.fcalib.action.AbstractExpertAction;
import de.tudresden.inf.tcs.fcalib.action.ChangeAttributeOrderAction;
import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.mockito.internal.matchers.Null;

public class ChangeAttributeOrderActionTest {


ChangeAttributeOrderAction changeAttributeOrderAction;

    @Before
    public void init()
    {
        changeAttributeOrderAction = new ChangeAttributeOrderAction();
    }

    @Test (expected = Exception.class)
    public void testActionPerformed()
    {
        ActionEvent actionEvent = new ActionEvent("s",2,"n");
        changeAttributeOrderAction.actionPerformed(actionEvent);

    }

    @Test (expected = Exception.class)
    public void testActionPerformedNull()
    {
        changeAttributeOrderAction.actionPerformed(null);

    }
}
