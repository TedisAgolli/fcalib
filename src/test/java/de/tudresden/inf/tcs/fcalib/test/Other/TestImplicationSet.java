package de.tudresden.inf.tcs.fcalib.test.Other;

import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.ImplicationSet;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;

import static junit.framework.TestCase.assertEquals;

public class TestImplicationSet {

    ImplicationSet<String> implicationSet;

    @Before
    public void init(){
        implicationSet = new ImplicationSet<>(new FormalContext<String,String>());
    }

    @Test(expected = NullPointerException.class)
    public void testClosureofNull(){

        implicationSet.closure(null);
    }

    @Test
    public void testClosureofEmptySet(){

        implicationSet.closure(new HashSet<>(Collections.EMPTY_SET));
    }

    @Ignore
    @Test
    public void testClosureofNonEmptySet(){
        //Todo: check if bug
        //Throws NullPointerException

        implicationSet.closure(new HashSet<>(Collections.nCopies(5,"test")));
    }

    //Todo: Test adding Implication before Closure


    @Test(expected = NullPointerException.class)
    public void testIsClosedNull(){
        implicationSet.isClosed(null);
    }

    @Test
    public void testIsClosedNonNull(){
        implicationSet.isClosed(Collections.EMPTY_SET);
    }

    @Ignore
    @Test
    public void testIsClosedNonEmpty(){
        implicationSet.isClosed(new HashSet<>(Collections.nCopies(5,"test"))) ;
    }

    @Test
    public void testNextClosureEmptySet(){
        implicationSet.nextClosure(Collections.EMPTY_SET);
    }

    @Test
    public void testNextCloseNonEmptySet() {
        implicationSet.nextClosure(new HashSet<>(Collections.nCopies(5,"")));
    }

    @Test
    public void testClosure(){
        implicationSet.add(new Implication<>());
        implicationSet.closure(Collections.EMPTY_SET);
    }

    @Test
    public void testAllClosures(){
        implicationSet.allClosures();
    }

    @Test(expected = NullPointerException.class)
    public void testAddNull(){
        implicationSet.add(null);
    }

    @Test
    public void testAddNonNull(){
        Implication<String> implication = new Implication<>();
        assertEquals(true,implicationSet.add(implication));
    }


    //Todo: Could be bug: Throws NullPointer
    @Test(expected = NullPointerException.class)
    public void testAddImplicationWithPremise(){
        HashSet<String> premise = new HashSet<>(Collections.nCopies(5,"premise"));
        Implication<String> implication = new Implication<>(premise,Collections.EMPTY_SET);
        assertEquals(true,implicationSet.add(implication));
    }
}

