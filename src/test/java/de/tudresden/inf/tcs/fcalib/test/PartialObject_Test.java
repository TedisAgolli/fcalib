package de.tudresden.inf.tcs.fcalib.test;

import java.util.ArrayList;
import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.change.ContextChange;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import de.tudresden.inf.tcs.fcalib.action.CounterExampleProvidedAction;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import de.tudresden.inf.tcs.fcalib.test.NoExpertFull;
import de.tudresden.inf.tcs.fcalib.change.HistoryManager;
import java.util.EventListener;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

public class PartialObject_Test {



    @Mock
    PartialObject<String, String> partialObjectMock;
    @Mock FCAImplication<String> fcaImplicationMock;
    @Mock Set<String> setMock;


    @Test
    public void respects_Test(){
        MockitoAnnotations.initMocks(this);
        doReturn(setMock).when(fcaImplicationMock).getPremise();
        //should return false because setMock does not share any attributes with partialObjectMock
        assertEquals(partialObjectMock.refutes(fcaImplicationMock),false);
    }


    @Test
    public void setName_Test(){
        PartialObject partialObject = new PartialObject("id01");
        //testing with lowercase string
        String nameInputLow = "lowercase";
        partialObject.setName(nameInputLow);
        assertEquals(partialObject.getName(),nameInputLow);
        //testing with uppercase string
        String nameInputUp = "UPPERCASE";
        partialObject.setName(nameInputUp);
        assertEquals(partialObject.getName(),nameInputUp);
        //testing with lowercase mixed with uppercasestring
        String nameInputLowUp = "lowUP";
        partialObject.setName(nameInputLowUp);
        assertEquals(partialObject.getName(),nameInputLowUp);
        //testing with empty string
        String nameInputEmpty = "";
        partialObject.setName(nameInputEmpty);
        assertEquals(partialObject.getName(),nameInputEmpty);
    }




}
