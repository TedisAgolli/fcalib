package de.tudresden.inf.tcs.fcalib.test.changes.test;

import java.util.ArrayList;
import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.change.ContextChange;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import de.tudresden.inf.tcs.fcalib.test.NoExpertFull;
import de.tudresden.inf.tcs.fcalib.change.HistoryManager;
import java.util.EventListener;
import org.junit.*;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;


public class HistoryManager_Test {

    /*
    *   push_test()
    *   checks if history manager push() function adds a change
    */
    @Test
    public void push_test(){
        try {
            HistoryManager hm = new HistoryManager();
            ContextChange change_1 = new NewImplicationChange(new FormalContext(), new Implication());
            hm.push(change_1);
            assertTrue(hm.size() == 1);
        } catch (Exception e){
            System.out.println("Test [push_test] Fails: " + e);
        }
    }


    /*
     *   undo_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void undo_test(){
        try {
            HistoryManager hm = new HistoryManager();
            ContextChange change_1 = new NewImplicationChange(new FormalContext(), new Implication());
            ContextChange change_2 = new NewImplicationChange(new FormalContext(), new Implication());
            ContextChange change_3 = new NewImplicationChange(new FormalContext(), new Implication());
            //store change strings to check after
            String change_1_string = change_1.toString();
            String change_2_string = change_2.toString();
            String change_3_string = change_3.toString();
            //push changes
            hm.push(change_1);
            hm.push(change_2);
            hm.push(change_3);
            //undo change 2
            hm.undo(change_2);
            assertTrue(hm.size() == 2 && hm.contains(change_2) != true);
        } catch (Exception e){
            System.out.println("Test [undo_test] Fails: " + e);
        }
    }



    /*
     *   undoAll_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void undoAll_test(){
        try {
            HistoryManager hm = new HistoryManager();
            ContextChange change_1 = new NewImplicationChange(new FormalContext(), new Implication());
            ContextChange change_2 = new NewImplicationChange(new FormalContext(), new Implication());
            ContextChange change_3 = new NewImplicationChange(new FormalContext(), new Implication());
            //push changes
            hm.push(change_1);
            hm.push(change_2);
            hm.push(change_3);
            //undo last all changes should result in empty arraylist
            hm.undoAll();
            assertTrue(hm.size() == 0);
        } catch (Exception e){
            System.out.println("Test [undoAll_test] Fails: " + e);
        }
    }



}
