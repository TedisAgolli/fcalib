package de.tudresden.inf.tcs.fcalib.test.Other;

import de.tudresden.inf.tcs.fcalib.FullObjectDescription;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TestFullObjectDescription {

    private FullObjectDescription<String> fullObjectDescription;

    @Before
    public void init(){
        fullObjectDescription = new FullObjectDescription<>();
    }

    @Test(expected = NullPointerException.class)
    public void testAddAttributesNull(){
        fullObjectDescription.addAttributes(null);
    }

    @Test
    public void testAddAttributesNotInList(){
        Set<String> attributes = new HashSet<>(Arrays.asList("1","2"));
        assertEquals(true,fullObjectDescription.addAttributes(attributes));
    }

    @Test
    public void testAddAttributesInList(){
        Set<String> attributes = new HashSet<>(Arrays.asList("1","2"));
        fullObjectDescription.addAttributes(attributes);
        assertEquals(false,fullObjectDescription.addAttributes(attributes));
    }

}
