package de.tudresden.inf.tcs.fcalib.test.Other;

import de.tudresden.inf.tcs.fcalib.Implication;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Collections;

import static junit.framework.TestCase.assertEquals;

@RunWith(JUnit4.class)
public class TestImplication {

    //Fixme: Tests added for mutation

    private Implication<String> implication;

    @Before
    public void init(){
        implication = new Implication<>();
    }

    @Test(expected = NullPointerException.class)
    public void testImplicationEqualsNull(){
        implication.equals(null);
    }

    @Test
    public void testImplicationEqualsSameTypesNoParametersTrue(){
        assertEquals(true,implication.equals(implication));
    }

    @Test
    public void testImplicationEqualsSameTypesWithParametersTrue(){
        implication = new Implication<>(Collections.singleton("1"),Collections.singleton("2"));
        Implication<String> implicationOther = new Implication<>(Collections.singleton("1"),Collections.singleton("2"));
        assertEquals(true,implication.equals(implicationOther));
    }



    //Todo: Empty Implications of different types are equal
    @Ignore
    @Test
    public void testImplicationEqualsDifferentTypesNoParameters(){
        Implication<Integer> implicationInt = new Implication<>();
        assertEquals(false,implicationInt.equals(implicationInt));
    }

    @Test
    public void testImplicationEqualsDifferentTypesWithParameters(){

        Implication<Integer> implicationInt = new Implication<>(Collections.singleton(1),Collections.singleton(2));
        implication = new Implication<>(Collections.singleton("1"),Collections.singleton("2"));
        assertEquals(false,implication.equals(implicationInt));
    }

    @Test
    public void testImplicationEmptyToString(){
        assertEquals(implication.getPremise() + " -> " + implication.getConclusion(),implication.toString());
    }

    @Test
    public void testImplicationNonEmptyToString(){
        implication = new Implication<>(Collections.singleton("1"),Collections.singleton("2"));
        assertEquals(implication.getPremise() + " -> " + implication.getConclusion(),implication.toString());
    }
}

