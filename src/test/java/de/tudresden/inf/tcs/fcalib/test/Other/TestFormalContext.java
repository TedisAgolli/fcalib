package de.tudresden.inf.tcs.fcalib.test.Other;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.FullObjectDescription;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(JUnit4.class)
public class TestFormalContext {


    @Spy
    private FormalContext formalContext;

    @Mock
    FullObject<String,String> mockFullObject;


    @Mock
    IndexedSet<String> indexedSet;

    @Mock
    Implication<String> mockImplication;

    private String testInput1,testinput2;
    private Set<String> attributes;
    private Set<FullObject<String,String>> objects;
    private FullObject<String,String> fullObjectforSet1, fullObjectforSet2;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        formalContext =Mockito.spy( new FormalContext());
        testInput1 = "testInput";
        testinput2 = "testInput2";
        attributes = new HashSet<> (Arrays.asList(testInput1,"test"));

        fullObjectforSet1 = new FullObject<>(testInput1);
        fullObjectforSet2 = new FullObject<>(testinput2);
        objects = new HashSet<>(Arrays.asList(fullObjectforSet1,fullObjectforSet2));

    }

    @Test(expected = NullPointerException.class)
    public void testAddObjectNull() throws IllegalObjectException {
        formalContext.addObject(null);
    }

    //Fixme: Mutation
    @Test
    public void testAddObjectNoException() throws IllegalObjectException {
        doReturn(testInput1).when(mockFullObject).getIdentifier();
        assertEquals(true,formalContext.addObject(mockFullObject));
    }

    @Test(expected = IllegalObjectException.class)
    public void testAddObjectAlreadyInList() throws IllegalObjectException{
        doReturn(testInput1).when(mockFullObject).getIdentifier();
        formalContext.addObject(mockFullObject);
        formalContext.addObject(mockFullObject);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetObjectAtIndexOutofBounds(){
        formalContext.getObjectAtIndex(100);
        formalContext.getObjectAtIndex(-1);
    }

    @Test
    public void testGetObjectAtIndex() throws IllegalObjectException {
        formalContext.addObject(mockFullObject);
        assertEquals(mockFullObject,formalContext.getObjectAtIndex(0));
    }

    @Test
    public void testGetObjectReturnNull() {
        assertEquals(null,formalContext.getObject(null));
    }
    @Test
    public void testGetObject() throws IllegalObjectException {
        doReturn(testInput1).when(mockFullObject).getIdentifier();
        formalContext.addObject(mockFullObject);
        assertEquals(mockFullObject,formalContext.getObject(testInput1));
    }

    @Test
    public void testRemoveObject() throws IllegalObjectException {
        doReturn(testInput1).when(mockFullObject).getIdentifier();
        formalContext.addObject(mockFullObject);

        assertEquals(true,formalContext.removeObject(testInput1));
    }


    @Test(expected = NullPointerException.class)
    public void testRemoveObjectNull() throws IllegalObjectException {
        formalContext.removeObject(null);
    }

    @Ignore
    @Test(expected = IllegalObjectException.class)
    public void testRemoveObjectNotInList() throws IllegalObjectException {
        formalContext.removeObject(testInput1);
    }

    @Test(expected = IllegalObjectException.class)
    public void testRemoveObjectNotInListWithReference() throws IllegalObjectException {
        formalContext.removeObject(mockFullObject);
    }

    @Test
    public void testRemoveObjectInListWithReference() throws IllegalObjectException {
        formalContext.addObject(mockFullObject);
        assertEquals(true,formalContext.removeObject(mockFullObject));
    }

    @Test
    public void testRemoveObjectInList() throws IllegalObjectException {
        doReturn(testInput1).when(mockFullObject).getIdentifier();
        formalContext.addObject(mockFullObject);
        assertEquals(true,formalContext.removeObject(testInput1));
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveAttributeFromObjectNull() throws IllegalObjectException {
        formalContext.removeAttributeFromObject(null,null);
    }

    @Test(expected = IllegalAttributeException.class)
    public void testRemoveAttributeDoesNotExist() throws IllegalObjectException {
        formalContext.removeAttributeFromObject(testInput1,testInput1);
    }
    @Test(expected = IllegalObjectException.class)
    public void testRemoveAttributeExists() throws IllegalObjectException {
        doReturn(indexedSet).when(formalContext).getAttributes();
        doReturn(true).when(indexedSet).contains(testInput1);
        formalContext.removeAttributeFromObject(testInput1,testInput1);

    }

    //Fixme: Fixed for mutation
    @Test
    public void testRemoveAttributeExistsandObjectExistsandContainsAttribute() throws IllegalObjectException {
        formalContext =Mockito.spy( new FormalContext());
        FullObjectDescription<String> fullObjectDescription = new FullObjectDescription<>(Collections.singleton(testInput1));

        doReturn(indexedSet).when(formalContext).getAttributes();
        doReturn(true).when(indexedSet).contains(testInput1);

        doReturn(true).when(formalContext).containsObject(testInput1);

        doReturn(mockFullObject).when(formalContext).getObject(testInput1);
        doReturn(fullObjectDescription).when(mockFullObject).getDescription();

        assertEquals(true,formalContext.removeAttributeFromObject(testInput1,testInput1));

    }

    @Test(expected = IllegalAttributeException.class)
    public void testRemoveAttributeExistsandObjectExistsandNotContainsAttribute() throws IllegalObjectException {
        formalContext =Mockito.spy( new FormalContext());
        FullObjectDescription<String> fullObjectDescription = new FullObjectDescription<>();

        doReturn(indexedSet).when(formalContext).getAttributes();
        doReturn(true).when(indexedSet).contains(testInput1);

        doReturn(true).when(formalContext).containsObject(testInput1);

        doReturn(mockFullObject).when(formalContext).getObject(testInput1);
        doReturn(fullObjectDescription).when(mockFullObject).getDescription();

        formalContext.removeAttributeFromObject(testInput1,testInput1);

    }


    @Test(expected = NullPointerException.class)
    public void testAddAttributetoObjectNull() throws IllegalObjectException {
        formalContext.addAttributeToObject(null,null);
    }

    @Test(expected = IllegalAttributeException.class)
    public void testAddAttributeDoesNotExist() throws IllegalObjectException {
        formalContext.addAttributeToObject(testInput1,testInput1);
    }
    @Test(expected = IllegalObjectException.class)
    public void testAddAttributeExists() throws IllegalObjectException {
        doReturn(indexedSet).when(formalContext).getAttributes();
        doReturn(true).when(indexedSet).contains(testInput1);
        formalContext.addAttributeToObject(testInput1,testInput1);

    }

    //Fixme: Fixed for mutation
    @Test(expected = IllegalAttributeException.class)
    public void testAddAttributeExistsandObjectExistsandContainsAttribute() throws IllegalObjectException {
        formalContext =Mockito.spy( new FormalContext());
        FullObjectDescription<String> fullObjectDescription = new FullObjectDescription<>(Collections.singleton(testInput1));

        doReturn(indexedSet).when(formalContext).getAttributes();
        doReturn(true).when(indexedSet).contains(testInput1);

        doReturn(true).when(formalContext).containsObject(testInput1);

        doReturn(mockFullObject).when(formalContext).getObject(testInput1);
        doReturn(fullObjectDescription).when(mockFullObject).getDescription();

        formalContext.addAttributeToObject(testInput1,testInput1);

    }

    //Fixme: Fixed for mutation
    @Test
    public void testAddAttributeExistsandObjectExistsandNotContainsAttribute() throws IllegalObjectException {
        formalContext =Mockito.spy( new FormalContext());
        FullObjectDescription<String> fullObjectDescription = new FullObjectDescription<>();

        doReturn(indexedSet).when(formalContext).getAttributes();
        doReturn(true).when(indexedSet).contains(testInput1);

        doReturn(true).when(formalContext).containsObject(testInput1);

        doReturn(mockFullObject).when(formalContext).getObject(testInput1);

        doReturn(fullObjectDescription).when(mockFullObject).getDescription();
        assertEquals(true,formalContext.addAttributeToObject(testInput1, testInput1));
    }


    //Fixme: Mutation
    @Test(expected = NullPointerException.class)
    public void testAddAttributeNull(){
        formalContext.addAttribute(null);
    }

    @Test(expected = IllegalAttributeException.class)
    public void testAddAttributeAlreadyInList(){
        formalContext.addAttribute(testInput1);
        formalContext.addAttribute(testInput1);
    }

    @Test
    public void testAddAttributeNotInList(){
        assertEquals(true,formalContext.addAttribute(testInput1));
    }


    //Fixme: Mutations
    @Test(expected = IllegalAttributeException.class)
    public void testAddAttributesAlreadyInList(){

        formalContext.addAttributes(attributes);
        formalContext.addAttributes(attributes);
    }

    @Test
    public void testAddAttributesNotInList(){
        assertEquals(true,formalContext.addAttributes(attributes));
    }


    //Fixme: Mutation
    @Test(expected = IllegalObjectException.class)
    public void testAddObjectsInList() throws IllegalObjectException {

        formalContext.addObjects(objects);
        formalContext.addObjects(objects);
    }

    @Test
    public void testAddObjectsNotInList() throws IllegalObjectException {
        assertEquals(true,formalContext.addObjects(objects));

    }

    @Test
    public void testObjecthasAttributeFalse(){
        FullObject<String,String> fullObject = new FullObject<>(testInput1);
        assertEquals(false,formalContext.objectHasAttribute(fullObject,testInput1));
    }

    @Test
    public void testObjecthasAttributeTrue(){
        FullObject<String,String> fullObject = new FullObject<>(testInput1, Collections.singleton(testInput1));
        assertEquals(true,formalContext.objectHasAttribute(fullObject,testInput1));
    }

    @Test
    public void testRefutesTrue() throws IllegalObjectException {

        formalContext.addObject(mockFullObject);
        doReturn(false).when(mockFullObject).respects(mockImplication);
        assertEquals(true,formalContext.refutes(mockImplication));
    }

    @Test
    public void testRefutesFalse() throws IllegalObjectException {

        formalContext.addObject(mockFullObject);
        doReturn(true).when(mockFullObject).respects(mockImplication);
        assertEquals(false,formalContext.refutes(mockImplication));
    }

    @Test
    public void testClearObjectsEmpty(){
        formalContext = new FormalContext();
        formalContext.clearObjects();
        assertEquals(Collections.EMPTY_SET,formalContext.getObjects());
    }

    @Test
    public void testClearObjectsNotEmpty() throws IllegalObjectException {
        formalContext.addObject(mockFullObject);
        formalContext.clearObjects();
        assertEquals(Collections.EMPTY_SET,formalContext.getObjects());
    }
}
