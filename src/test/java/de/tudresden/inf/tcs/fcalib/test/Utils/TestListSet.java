package de.tudresden.inf.tcs.fcalib.test.Utils;

import de.tudresden.inf.tcs.fcalib.utils.ListSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TestListSet {

    private ListSet<String> listSet;
    private String testInput = "test";


    @Test
    public void testListSetWithCollections(){

        listSet = new ListSet<>(Collections.EMPTY_LIST);
        assertEquals(0,listSet.size());
    }

    //testAdd

    @Test(expected = NullPointerException.class )
    public void testAddNull(){
        listSet = new ListSet<>();
        listSet.add(null);
    }

    @Test
    public void testAddObject(){
        listSet = new ListSet<>();
        listSet.add("test");
    }


    @Test
    public void testAddObjectNotInList(){
        listSet = new ListSet<>();
        assertEquals(true,listSet.add("test"));
    }

    @Test
    public void testAddObjectInList(){
        listSet = new ListSet<>(Collections.singletonList(testInput));
        assertEquals(false,listSet.add(testInput));
    }

    //addCollection

    @Test(expected = NullPointerException.class)
    public void testAddCollectionNull(){
        listSet = new ListSet<>();
        listSet.addAll(null);
    }

    @Test
    public void testAddCollectionEmpty(){
        listSet = new ListSet<>();
        assertEquals(false,listSet.addAll(Collections.EMPTY_LIST));
    }

    @Test
    public void testAddCollectionNonEmpty(){
        listSet = new ListSet<>();
        assertEquals(true,listSet.addAll(Collections.singleton(testInput)));
    }


    //contains

    @Test(expected = NullPointerException.class)
    public void testContainsNull(){
        listSet = new ListSet<>();
        listSet.contains(null);
    }

    //containsAll

    @Test(expected = NullPointerException.class)
    public void testContainsAllNull(){
        listSet = new ListSet<>();
        listSet.containsAll(null);
    }

    @Test
    public void testContainsAll(){
        listSet = new ListSet<>();
        listSet.containsAll(Collections.singleton(testInput));
    }

    @Test
    public void testContainsAllNotInSet() {
        listSet = new ListSet<>(Collections.singleton("1"));
        assertEquals(false,listSet.containsAll(Collections.singleton(testInput)));
    }

    @Test
    public void testContainsAllInSet() {
        listSet = new ListSet<>(Collections.singleton(testInput));
        assertEquals(true,listSet.containsAll(Collections.singleton(testInput)));
    }

    //remove

    @Test(expected = NullPointerException.class)
    public void testRemoveNull(){
        listSet = new ListSet<>();
        listSet.remove(null);
    }

    @Test
    public void testRemoveNonNull(){
        listSet = new ListSet<>();
        listSet.remove(testInput);
    }

    @Test
    public void testRemoveNotInList(){
        listSet = new ListSet<>();
        assertEquals(false,listSet.remove(testInput));
    }

    @Test
    public void testRemoveInList(){
        listSet = new ListSet<>(Collections.singleton(testInput));
        assertEquals(true,listSet.remove(testInput));
    }


    //removeAll

    @Test(expected = NullPointerException.class)
    public void testRemoveAllNull(){
        listSet = new ListSet<>();
        listSet.removeAll(null);
    }

    @Test
    public void testRemoveAllNonNull(){
        listSet = new ListSet<>();
        listSet.removeAll(Collections.singleton(testInput));
    }

    @Test
    public void testRemoveAllNotInList(){
        listSet = new ListSet<>();
        assertEquals(false,listSet.removeAll(Collections.singleton(testInput)));
    }

    @Test
    public void testRemoveAllInList(){
        listSet = new ListSet<>(Collections.singleton(testInput));
        assertEquals(true,listSet.removeAll(Collections.singleton(testInput)));
    }

    //testRetainAll

    @Test(expected = NullPointerException.class)
    public void testRetainAllNull(){
        listSet = new ListSet<>(Collections.singleton(testInput));
        listSet.retainAll(null);
    }

    @Test
    public void testRetainAllNonNull(){
        listSet = new ListSet<>(Collections.singleton(testInput));
        listSet.retainAll(Collections.singleton(testInput));
    }

    @Test
    public void testRetainAllNoChange(){
        listSet = new ListSet<>(Collections.singleton(testInput));
        assertEquals(false,listSet.retainAll(Collections.singleton(testInput)));
    }

    @Test
    public void testRetainAllChanged(){
        listSet = new ListSet<>(Collections.singleton(testInput));
        assertEquals(true,listSet.retainAll(Collections.singleton("1")));
    }

    //toArray

    @Test(expected = NullPointerException.class)
    public void testToArrayNull(){
        listSet = new ListSet<>();
        listSet.toArray(null);
    }

    @Test
    public void testToArrayNonNull(){
        listSet =  new ListSet<>();
        listSet.toArray(new String[1]);
    }

    @Test
    public void testToArraySameSize(){
        listSet =  new ListSet<>(Collections.nCopies(3,testInput));
        assertEquals(3,listSet.toArray(new String[3]).length);
    }
    @Test
    public void testToArrayDifferentSizeNoException(){
        listSet =  new ListSet<>(Collections.nCopies(3,testInput));
        listSet.toArray(new String[1]);
    }


    //getIndexOf

    @Test
    public void testGetIndexOfNull(){
        listSet = new ListSet<>();
        listSet.getIndexOf(null);
    }

    @Test
    public void testGetIndexOfObjectNotinList(){
        listSet = new ListSet<>();
        assertEquals(-1,listSet.getIndexOf(testInput));
    }

    @Test
    public void testGetIndexOfObjectinList(){
        listSet = new ListSet<>(Collections.singleton(testInput));
        assertEquals(0,listSet.getIndexOf(testInput));
    }


}
