package de.tudresden.inf.tcs.fcalib.test.Action;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.action.CounterExampleProvidedAction;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.w3c.dom.css.Counter;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import java.awt.event.ActionEvent;

import static org.mockito.Mockito.doReturn;


public class CounterExampleProvidedActionTest {


    @Test(expected = Exception.class)
    public void testActionPerformedIf(){

        CounterExampleProvidedAction counterExampleProvidedAction = new CounterExampleProvidedAction(new FormalContext(), new Implication(), (FCAObject) null);
        ActionEvent actionEvent = new ActionEvent(new Object(), 10, "s");
        counterExampleProvidedAction.actionPerformed(actionEvent);
}


    @Test(expected = Exception.class)
    public void testActionPerformedElse(){

        CounterExampleProvidedAction counterExampleProvidedAction = new CounterExampleProvidedAction(new FormalContext(), new Implication(), (FCAObject) null);
        ActionEvent actionEvent = new ActionEvent(new Object(), 10, "s");
        actionEvent = null;
        counterExampleProvidedAction.actionPerformed(actionEvent);
    }

    @Test(expected = Exception.class)
    public void testActionPerformedException(){

        CounterExampleProvidedAction counterExampleProvidedAction = new CounterExampleProvidedAction(new FormalContext(), new Implication(), (FCAObject) null);
        ActionEvent actionEvent = new ActionEvent(new Object(), 10, "s");
        counterExampleProvidedAction.actionPerformed(actionEvent);
    }
}


