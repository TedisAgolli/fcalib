package de.tudresden.inf.tcs.fcalib.test;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcalib.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.change.ContextChange;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.action.CounterExampleProvidedAction;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import de.tudresden.inf.tcs.fcalib.test.NoExpertFull;
import de.tudresden.inf.tcs.fcalib.change.HistoryManager;
import java.util.EventListener;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

public class PartialObjectDescription_Test {

    @Test
    public void addAttribute_Test(){
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        partialObjectDescription.addAttribute("Attr");
        partialObjectDescription.addAttribute("");
        partialObjectDescription.addAttribute(null);

    }


    @Test (expected = IllegalArgumentException.class)
    public void addAttribute_Test_negated(){
        //added negated attribute should throw exception
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        partialObjectDescription.addNegatedAttribute("Attr");
        partialObjectDescription.addAttribute("Attr");
    }


    @Test
    public void addNegatedAttribute_Test(){
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        partialObjectDescription.addNegatedAttribute("Attr");
        partialObjectDescription.addNegatedAttribute("");
        partialObjectDescription.addNegatedAttribute(null);
    }


    @Test (expected = IllegalArgumentException.class)
    public void addNegatedAttribute_Test_existing(){
        //existing attribute should throw exception
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        partialObjectDescription.addAttribute("Attr");
        partialObjectDescription.addNegatedAttribute("Attr");
    }


    @Test
    public void containsNegatedAttribute_Test(){
        //existing attribute should throw exception
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        partialObjectDescription.addNegatedAttribute("Attr1");
        assertEquals(partialObjectDescription.containsNegatedAttribute("Attr1"),true);
    }



    @Mock
    Set<String> setMock;

    @Test (expected = NullPointerException.class)
    public void containsNegatedAttributes_Test(){
        //existing attribute should throw exception
        PartialObjectDescription partialObjectDescription = new PartialObjectDescription();
        partialObjectDescription.containsNegatedAttributes(setMock);
    }


}
