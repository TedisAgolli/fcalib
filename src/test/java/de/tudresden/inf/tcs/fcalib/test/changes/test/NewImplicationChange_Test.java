package de.tudresden.inf.tcs.fcalib.test.changes.test;

import de.tudresden.inf.tcs.fcaapi.*;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.junit.Before;
import org.junit.Test;

import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

public class NewImplicationChange_Test {


    private Test_Mocks test_mocks = new Test_Mocks();
    private Context context = test_mocks.context;


    @Mock
    private FCAImplication<String> implication;

    @Mock
    private FCAObject<String,String> fcaObject;

    @Mock
    private Context<String,String,FCAObject<String,String>> contextMock;


    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }
    /*
     *   undo_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void getType_test() {

        try {
            NewImplicationChange imp_1 = new NewImplicationChange(contextMock, implication);
            assertTrue(imp_1.getType() == 1);
        } catch (Exception e){
            System.out.println("Test [getType_test] Fails: " + e);
        }
    }


    /*
     *   undo_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test()
    public void undo_test(){
            NewImplicationChange imp_1 = new NewImplicationChange(contextMock, implication);
            Set<FCAImplication<String>> fcaImplications = new HashSet<>(Arrays.asList(implication));

            doReturn(fcaImplications).when(contextMock).getImplications();
            imp_1.undo();
            assertEquals(0,fcaImplications.size());
    }


    /*
     *   getImplication_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void getImplication_test(){
        try {
            NewImplicationChange imp_1 = new NewImplicationChange(context, implication);
            FCAImplication imp_compare = imp_1.getImplication();
            assertTrue(imp_compare == implication);
        } catch (Exception e){
            System.out.println("Test [getImplication_test] Fails: " + e);
        }
    }


}
