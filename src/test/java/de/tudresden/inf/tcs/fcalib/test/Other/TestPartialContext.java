package de.tudresden.inf.tcs.fcalib.test.Other;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import de.tudresden.inf.tcs.fcalib.PartialObjectDescription;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Null;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(JUnit4.class)
public class TestPartialContext {

    private PartialContext<String,String, PartialObject<String,String>> partialContext;
    private String objectID;


    @Mock
    PartialObject<String,String> mockPartialObject;

    @Mock
    PartialObjectDescription<String> mockPartialObjectDescription;

    @Mock
    Implication<String> mockImplication;

    @Mock
    IndexedSet<String> indexedSet;
    
    

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        partialContext =Mockito.spy(new PartialContext<>());
        objectID = "ID";
    }

    @Ignore
    @Test(expected = NullPointerException.class)
    public void testGetObjectNullString(){
        //TODO Maybe should throw exception?
        partialContext.getObject(null);
    }

    @Test
    public void testGetObjectEmptyString(){
        assertEquals(null,partialContext.getObject(""));
    }

    @Test
    public void testGetObjectExists(){
        PartialObject<String,String> partialObject = new PartialObject<String, String>(objectID,Collections.EMPTY_SET);

        partialContext.addObject(partialObject);
        assertEquals(partialObject,partialContext.getObject(objectID));
    }

    @Test
    public void testRefutesNull(){
        //Todo: Should throw a NullPointer?
        partialContext.refutes(null);
    }

    @Test
    public void testRefutesNonNull(){
        Implication<String> implication = new Implication<>();
        partialContext.refutes(implication);
    }

    //Fixme: Mutation
    @Test
    public void testRefutesTrue(){
        doReturn(true).when(mockPartialObject).refutes(mockImplication);
        partialContext.addObject(mockPartialObject);
        assertEquals(true,partialContext.refutes(mockImplication));
    }

    //Fixme: Mutation
    @Test
    public void testRefutesFalse(){
        doReturn(false).when(mockPartialObject).refutes(mockImplication);
        partialContext.addObject(mockPartialObject);
        assertEquals(false,partialContext.refutes(mockImplication));
    }

    @Test(expected = NullPointerException.class)
    public void testIsCounterExampleValidNull(){
        partialContext.isCounterExampleValid(null,null);
    }

    @Test
    public void testIsCounterExampleValidNonNull(){
        partialContext.isCounterExampleValid(mockPartialObject,mockImplication);
    }

    @Test(expected = NullPointerException.class)
    public void testAddObjectNull(){
        partialContext.addObject(null);
    }

    @Test
    public void testAddObjectNonNull(){
        partialContext.addObject(new PartialObject<String,String>(objectID,Collections.EMPTY_SET));
    }

    @Test
    public void testAddObjectReturnTrue(){
        assertEquals(true,partialContext.addObject(new PartialObject<String,String>(objectID,Collections.EMPTY_SET)));
    }

    @Test
    public void testAddObjectReturnFalse(){
        PartialObject<String,String> partialObject =new PartialObject<String,String>(objectID,Collections.EMPTY_SET);
        partialContext.addObject(partialObject);
        assertEquals(false,partialContext.addObject(partialObject));
    }


    @Test(expected = NullPointerException.class)
    public void testAddAttributetoObjectNull() throws IllegalObjectException {
        partialContext.addAttributeToObject(null,null);
    }

    @Test(expected = IllegalAttributeException.class)
    public void testAddAttributeDoesNotExist() throws IllegalObjectException {
        partialContext.addAttributeToObject(objectID,objectID);
    }
    @Test(expected = IllegalObjectException.class)
    public void testAddAttributeExists() throws IllegalObjectException {
        doReturn(indexedSet).when(partialContext).getAttributes();
        doReturn(true).when(indexedSet).contains(objectID);
        partialContext.addAttributeToObject(objectID,objectID);

    }

    @Test(expected = IllegalAttributeException.class)
    public void testAddAttributeExistsandObjectExistsandContainsAttribute() throws IllegalObjectException {

        partialContext =Mockito.spy(new PartialContext<>());
        PartialObjectDescription<String> partialObjectDescription = new PartialObjectDescription<>(Collections.singleton(objectID));

        doReturn(indexedSet).when(partialContext).getAttributes();
        doReturn(true).when(indexedSet).contains(objectID);

        doReturn(true).when(partialContext).containsObject(objectID);

        doReturn(mockPartialObject).when(partialContext).getObject(objectID);
        doReturn(partialObjectDescription).when(mockPartialObject).getDescription();

        partialContext.addAttributeToObject(objectID,objectID);

    }

    //Fixme: Mutation
    @Test
    public void testAddAttributeExistsandObjectExistsandNotContainsAttribute() throws IllegalObjectException {

        partialContext =Mockito.spy(new PartialContext<>());
        PartialObjectDescription<String> partialObjectDescription = new PartialObjectDescription<>();

        doReturn(indexedSet).when(partialContext).getAttributes();
        doReturn(true).when(indexedSet).contains(objectID);

        doReturn(true).when(partialContext).containsObject(objectID);

        doReturn(mockPartialObject).when(partialContext).getObject(objectID);
        doReturn(partialObjectDescription).when(mockPartialObject).getDescription();

        assertEquals(true,partialContext.addAttributeToObject(objectID,objectID));
    }

    @Test
    public void testRemoveObject() throws IllegalObjectException {
        doReturn(objectID).when(mockPartialObject).getIdentifier();
        partialContext.addObject(mockPartialObject);
        TestCase.assertEquals(true,partialContext.removeObject(objectID));
    }


    @Ignore
    @Test(expected = IllegalObjectException.class)
    public void testRemoveObjectEmptyStringIllegalObject() throws IllegalObjectException {
        partialContext.removeObject("");
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveObjectEmptyString() throws IllegalObjectException {
        partialContext.removeObject("");
    }

    //Could be bug. Object is not in list but you get NullPointerException
    @Test(expected = NullPointerException.class)
    public void testRemoveObjectNotInList() throws IllegalObjectException {
        partialContext.removeObject(objectID);
    }

    @Test
    public void testRemoveObjectInList() throws IllegalObjectException {
        doReturn(objectID).when(mockPartialObject).getIdentifier();
        partialContext.addObject(mockPartialObject);
        TestCase.assertEquals(true,partialContext.removeObject(objectID));
    }


    @Test
    public void testRemoveObjectTrueWithReference() throws IllegalObjectException {
        partialContext.addObject(mockPartialObject);
        partialContext.removeObject(mockPartialObject);
    }

    @Test(expected = IllegalObjectException.class)
    public void testRemoveObjectFalseWithReference() throws IllegalObjectException {
        partialContext.removeObject(mockPartialObject);
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveAttributeFromObjectNull() throws IllegalObjectException {
        partialContext.removeAttributeFromObject(null,null);
    }

    @Test(expected = IllegalAttributeException.class)
    public void testRemoveAttributeDoesNotExist() throws IllegalObjectException {
        partialContext.removeAttributeFromObject(objectID,objectID);
    }
    @Test(expected = IllegalObjectException.class)
    public void testRemoveAttributeExists() throws IllegalObjectException {
        doReturn(indexedSet).when(partialContext).getAttributes();
        doReturn(true).when(indexedSet).contains(objectID);
        partialContext.removeAttributeFromObject(objectID,objectID);

    }

    @Test
    public void testRemoveAttributeExistsandObjectExistsandContainsAttribute() throws IllegalObjectException {

        partialContext =Mockito.spy(new PartialContext<>());
        PartialObjectDescription<String> partialObjectDescription = new PartialObjectDescription<>(Collections.singleton(objectID));

        doReturn(indexedSet).when(partialContext).getAttributes();
        doReturn(true).when(indexedSet).contains(objectID);

        doReturn(true).when(partialContext).containsObject(objectID);

        doReturn(mockPartialObject).when(partialContext).getObject(objectID);
        doReturn(partialObjectDescription).when(mockPartialObject).getDescription();

        assertEquals(true,partialContext.removeAttributeFromObject(objectID,objectID));

    }

    @Test(expected = IllegalAttributeException.class)
    public void testRemoveAttributeExistsandObjectExistsandNotContainsAttribute() throws IllegalObjectException {

        partialContext =Mockito.spy(new PartialContext<>());
        PartialObjectDescription<String> partialObjectDescription = new PartialObjectDescription<>();

        doReturn(indexedSet).when(partialContext).getAttributes();
        doReturn(true).when(indexedSet).contains(objectID);

        doReturn(true).when(partialContext).containsObject(objectID);

        doReturn(mockPartialObject).when(partialContext).getObject(objectID);
        doReturn(partialObjectDescription).when(mockPartialObject).getDescription();

        partialContext.removeAttributeFromObject(objectID,objectID);

    }


    @Test
    public void testDoublePrimeEmptySet(){
        partialContext.doublePrime(Collections.EMPTY_SET);
    }


    @Test
    public void testDoublePrimeNonEmptySet(){
        partialContext.doublePrime(new HashSet<>(Collections.nCopies(5,objectID)));
    }

    @Test
    public void testDoublePrimeRemovesNegatedAttr() {

        HashSet<String> attributes = new HashSet<>(Collections.nCopies(5,"attributes"));
        HashSet<String> negativeAttributes = new HashSet<>(Collections.nCopies(5,"negative attributes"));

        partialContext.addAttributes(attributes);
        partialContext.addAttributes(negativeAttributes);

        PartialObject<String,String> partialObject = new PartialObject<>(objectID, attributes,negativeAttributes);
        partialContext.addObject(partialObject);

        Set<String> attr = partialContext.doublePrime(attributes);

        assertEquals(true,attr.containsAll(attributes) && !attr.containsAll(negativeAttributes) );
    }


    @Test(expected = NullPointerException.class)
    public void testHasAttributeNull(){
        partialContext.objectHasAttribute(null,null);
    }

    @Test
    public void testHasAttributeNonNullFalse(){
        PartialObject<String,String> partialObject = new PartialObject<>(objectID);
        assertEquals(false,partialContext.objectHasAttribute(partialObject,objectID));
    }

    @Test
    public void testHasAttributeTrue(){
        PartialObject<String,String> partialObject = new PartialObject<>(objectID,Collections.singleton(objectID));
        assertEquals(true,partialContext.objectHasAttribute(partialObject,objectID));
    }


    @Test(expected = NullPointerException.class)
    public void testHasNegatedAttributeNull(){
        partialContext.objectHasNegatedAttribute(null,null);
    }

    @Test
    public void testHasNegatedAttributeNonNullFalse(){
        PartialObject<String,String> partialObject = new PartialObject<>(objectID,Collections.singleton(objectID));
        assertEquals(false,partialContext.objectHasNegatedAttribute(partialObject,objectID));
    }

    @Test
    public void testHasNegatedAttributeNonNullTrue(){
        PartialObject<String,String> partialObject = new PartialObject<>(objectID,Collections.EMPTY_SET,Collections.singleton(objectID));
        assertEquals(true,partialContext.objectHasNegatedAttribute(partialObject,objectID));
    }





}

