package de.tudresden.inf.tcs.fcalib.test.changes.test;


import de.tudresden.inf.tcs.fcaapi.*;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import org.junit.Test;

import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class NewObjectChange_Test {



    private Test_Mocks test_mocks = new Test_Mocks();
    private Context context = test_mocks.context;
    private FCAObject fca_obj = test_mocks.fca_obj;



    /*
     *   getObject_test()
     *   checks if object given to NewObjectChange is equal to that returned by getObject()
     */
    @Test
    public void getObject_test(){
        try {
            NewObjectChange new_obj_change = new NewObjectChange(context, fca_obj);
            //given object should match object put in
            assertTrue(new_obj_change.getObject() == fca_obj);
        } catch (Exception e){
            System.out.println("Test [getObject_test] Fails: " + e);
        }
    }


    /*
     *   undo_test()
     *   checks if object given to NewObjectChange is equal to that returned by getObject()
     */
    @Test
    public void undo_test(){
        try {
            NewObjectChange new_obj_change = new NewObjectChange(context, fca_obj);
            new_obj_change.undo();
            assertTrue(new_obj_change.getObject().getDescription().equals("null"));
        } catch (Exception e){
            System.out.println("Test [undo_test] Fails: " + e);
        }
    }


    /*
     *   getType_test()
     *   checks if object given to NewObjectChange is equal to that returned by getObject()
     */
    @Test
    public void getType_test(){
        try {
            NewObjectChange new_obj_change = new NewObjectChange(context, fca_obj);
            assertTrue(new_obj_change.getType() == 2);
        } catch (Exception e){
            System.out.println("Test [getType_test] Fails: " + e);
        }
    }

}
