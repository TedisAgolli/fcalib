package de.tudresden.inf.tcs.fcalib.test.Action;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

import de.tudresden.inf.tcs.fcalib.action.AbstractExpertAction;
import de.tudresden.inf.tcs.fcalib.action.StopExplorationAction;
import org.junit.*;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.mockito.internal.matchers.Null;
import static org.junit.Assert.*;

public class StopExplorationActionTest {

    StopExplorationAction stopExplorationAction;

    @Before
    public void init()
    {
        stopExplorationAction = new StopExplorationAction();
    }


    @Test
    public void testActionPerformed()
            throws Exception {

        ActionEvent actionEvent = new ActionEvent(new Object(), 10, "s");
        actionEvent=null;
        stopExplorationAction.actionPerformed(actionEvent);

    }


}
