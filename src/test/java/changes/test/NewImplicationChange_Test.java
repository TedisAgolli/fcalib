package changes.test;

import de.tudresden.inf.tcs.fcaapi.*;
import de.tudresden.inf.tcs.fcaapi.action.ExpertAction;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.Implication;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

public class NewImplicationChange_Test {


    @Mock
    private FCAImplication<String> implication;

   @Mock
   private FCAObject<String,String> fcaObject;

    @Mock
    private Context<String,String,FCAObject<String,String>> contextMock;

    @Mock
    private Set<String> setMock;

    @Before
    public void init(){
                MockitoAnnotations.initMocks(this);
            }




    /*
     *   undo_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void getType_test() {
        try {
            NewImplicationChange imp_1 = new NewImplicationChange(contextMock, implication);
            assertTrue(imp_1.getType() == 1);
        } catch (Exception e){
            System.out.println("Test [getType_test] Fails: " + e);
        }
    }


    /*
     *   undo_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void undo_test(){
        doReturn(setMock).when(contextMock).getImplications();
        NewImplicationChange imp_1 = new NewImplicationChange(contextMock, implication);
        imp_1.undo();
    }


    /*
     *   getImplication_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void getImplication_test(){
        NewImplicationChange imp_1 = new NewImplicationChange(contextMock, implication);
        FCAImplication imp_compare = imp_1.getImplication();
        assertEquals(implication,imp_compare);
    }


}
