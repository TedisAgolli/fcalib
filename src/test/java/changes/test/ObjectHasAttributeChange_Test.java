package changes.test;

import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class ObjectHasAttributeChange_Test {


    private Test_Mocks test_mocks = new Test_Mocks();
    private FCAObject fca_obj = test_mocks.fca_obj;


    /*
     *   getObject_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void getObject_test() {
        try {
            ObjectHasAttributeChange obj_has_attr_change = new ObjectHasAttributeChange(fca_obj,null);
            //should equal object passed in
            assertTrue(obj_has_attr_change.getObject() == fca_obj);
        } catch (Exception e){
            System.out.println("Test [getObject_test] Fails: " + e);
        }
    }



    /*
     *   getAttribute_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void getAttribute_test() {
        try {
            ObjectHasAttributeChange obj_has_attr_change = new ObjectHasAttributeChange(fca_obj,null);
            //should equal object null passed in
            assertTrue(obj_has_attr_change.getAttribute() == null);
        } catch (Exception e){
            System.out.println("Test [getAttribute_test] Fails: " + e);
        }
    }



    /*
     *   getType_test()
     *   checks if object given to NewObjectChange is equal to that returned by getObject()
     */
    @Test
    public void getType_test(){
        try {
            ObjectHasAttributeChange obj_has_attr_change = new ObjectHasAttributeChange(fca_obj,null);
            assertTrue(obj_has_attr_change.getType() == 0);
        } catch (Exception e){
            System.out.println("Test [getType_test] Fails: " + e);
        }
    }



    /*
     *   undo_test()
     *   checks if object given to NewObjectChange is equal to that returned by getObject()
     */
    @Test
    public void undo_test(){
        try {
            ObjectHasAttributeChange obj_has_attr_change = new ObjectHasAttributeChange(fca_obj,null);
            obj_has_attr_change.undo();
            assertTrue(obj_has_attr_change.getObject().getDescription().equals("null"));
        } catch (Exception e){
            System.out.println("Test [undo_test] Fails: " + e);
        }
    }

}
