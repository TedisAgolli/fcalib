package changes.test;

import java.util.ArrayList;
import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.change.ContextChange;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import de.tudresden.inf.tcs.fcalib.action.CounterExampleProvidedAction;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;
import de.tudresden.inf.tcs.fcalib.test.NoExpertFull;
import de.tudresden.inf.tcs.fcalib.change.HistoryManager;
import java.util.EventListener;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;


public class HistoryManager_Test {


    @Mock
    private ContextChange<String> contextChangeMock;


    /*
    *   push_test()
    *   checks if history manager push() function adds a change
    */
    @Test
    public void push_test(){
        HistoryManager hm = new HistoryManager();
        hm.push(contextChangeMock);
        assertTrue(hm.size() == 1);
    }


    @Mock
    private ContextChange<String> change_1;
    @Mock
    private ContextChange<String> change_2;
    @Mock
    private ContextChange<String> change_3;


    /*
     *   undo_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test //(expected = Exception.class)
    public void undo_test(){
        MockitoAnnotations.initMocks(this);
            HistoryManager hm = new HistoryManager();
            //push changes
            hm.push(change_1);
            hm.push(change_2);
            hm.push(change_3);
            doNothing().when(change_2).undo();
            //undo change 2
            hm.undo(change_2);
            assertTrue(hm.size() == 2 && hm.contains(change_2) != true);
    }



    @Mock
    private ContextChange<String> contextChangeMock1;
    @Mock
    private ContextChange<String> contextChangeMock2;


    /*
     *   undoAll_test()
     *   checks if history manager undo() removes a given change from changes in list
     */
    @Test
    public void undoAll_test(){
        MockitoAnnotations.initMocks(this);
            HistoryManager hm = new HistoryManager();
            //push changes
            hm.push(contextChangeMock1);
            hm.push(contextChangeMock2);
            System.out.println(hm.size());
            doNothing().when(contextChangeMock1).undo();
            doNothing().when(contextChangeMock2).undo();
            //undo last all changes should result in empty arraylist
            hm.undoAll();
            assertTrue(hm.size() == 0);
    }



}
